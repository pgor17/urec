OBJ =  tools.o rtree.o urtree.o
CFLAGS = -Wall -c 
CC = g++ -O3
LFLAGS =  -Wall 

all: urec treeman

rtree.o: rtree.cpp tree.h rtree.h
tools.o: tools.cpp tools.h rtree.h tree.h urtree.h
treeman.o: treeman.cpp tools.h rtree.h tree.h urtree.h
urec.o: urec.cpp tools.h rtree.h tree.h urtree.h
urtree.o: urtree.cpp urtree.h rtree.h tree.h

%.o : %.cpp
	$(CC) $(CFLAGS) -o $@ $<

urec: $(OBJ) urec.o
	$(CC) $(LFLAGS) -o $@ $(OBJ) $@.o

treeman: $(OBJ) treeman.o
	$(CC) $(LFLAGS) -o $@ $(OBJ) $@.o

clean:
	rm -f *.o $(TARGET) *.old *~ x *.log

tgz: 
	tar czvf urec.tgz *.cpp *.h Makefile README*

bin:
	tar czvf urec tt README*


