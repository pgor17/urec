

/************************************************************************
 Unrooted REConciliation
 Written by P.Gorecki.
 Permission is granted to copy and use this program provided no fee is
 charged for it and provided that this copyright notice is not removed.
 *************************************************************************/

/****
 13/4/17 New options added: -a54 for ume
 1/2/21 dlplateau midpoint rooting
****/

const char *version="2.04";

#include <string.h>
#include <sys/time.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "tools.h"

#define OPT_RECDETAILS 1
#define OPT_RECINFO 2
#define OPT_PRINTROOTINGS 4
#define OPT_RECMINROOTING 8
#define OPT_RECTREECOSTDETAILS 16
#define OPT_PRINTGENE 32
#define OPT_PRINTSPECIES 64
#define OPT_RECMINCOST 128

#define OPT_SUMMARYTOTAL 256
#define OPT_SUMMARYDISTRIBUTIONS 512
#define OPT_SUMMARYDLTOTAL (1<<12)
#define OPT_TREEDISTRIBUTIONS (1<<13)
#define OPT_VOTING (1<<14)
#define OPT_BYCOST (1<<15)
#define OPT_RANDUNIQUE (1<<16)
#define OPT_RECMUTCOST (1<<17)

#define OPT_RECMINROOTINGEXT (1<<18)
#define OPT_CLUSTERS (1<<19)
#define OPT_RECDETAILSEXT (1<<20)

#define OPT_NNI (1<<25)
#define OPT_RSORT (1<<26)

#define MIDPOINTPLATEAUROOTINGSLENGTHS 1
#define MIDPOINTPLATEAUROOTINGSEDGES 2
#define MIDPOINTROOTINGSLENGTHS 3
#define MIDPOINTROOTINGSEDGES 4

#define MIDPOINT_ONEOPTROOTING 1     
#define MIDPOINT_EXTENDED 2
#define MIDPOINT_ALLOPTROOTINGS 4
#define ALLROOTINGS 8


#define RT_BYPLATEAU 1
#define RT_BYEDGES 2
#define RT_OPTROOTINGS 4
#define RT_ONEROOTING 8 
#define RT_MIDPOINT 16
#define RT_ALL 32
#define RT_ROOTSPLIT 64
#define RT_EXTENDED 1024
#define RT_CONNECTIVITY 2048

extern int gspos;
extern int gsid;
extern char* gsdelim;
int normalizecost=0;

bool printedgeids = false;


#define PROOTINGS(ur) cout << "(" << *ur->smprooted() << "," << *ur->p()->smprooted() << ") "
#define PROOTINGSCOST(ur,dlu) PROOTINGS(ur) <<  dlu.dup << " "<< dlu.loss << " " <<  dlu.dup+dlu.loss<<endl;


int adjacent(UNode *a, UNode *b)
{
  if (a == b) return 1;
  if (a->leaf()) return 0;
  UNode3 *ap = (UNode3*)a;
  return ap->r() == b || ap->r()->r() == b;
}

int usage(int argc, char **argv) {
  cout << " Unrooted REConciliation " << version <<". (C) P.Gorecki 2005-2021" << endl;

  cout << " Usage: " << argv[0]; 
  cout << " [options] [-g GENE(S)TREESTR] [-s SPECIESTREE(S)STR] [[-G] FILE_GENETREES] [-S FILE_SPECIESTREES] ... ]";

  cout << "Reading trees. Use - instead of FILE_* to read from stdin" << endl;  

  cout << endl;
  cout << "Input trees" << endl;
  cout << " -s STR: species trees fromom a string" << endl;
  cout << " -g STR: unrooted gene trees from a string" << endl;
  cout << " -t STR: interleaved format: gene_tree <EOLN> species_tree <EOLN> ..." << endl;  

  cout << " -S FILE_SPECIESTREES: species trees from a file; <;> or <EOLN> separated" << endl;
  cout << " -G FILE_GENETREES: unrooted gene trees from a file" << endl;
  cout << " -T FILE_INTERLEAVED: file in interleaved format" << endl;

  cout << "Alternatively gene tree files can be defined by extra arguments" << endl;
  cout << endl;

  cout << "General tree options" << endl;
  cout << " -p - print a gene tree" << endl;
  cout << " -P - print a species tree" << endl;  
  cout << endl;

  cout << "Unrooted Reconciliation" << endl;   
  cout << " -u OPT - reconcile unrooted a set of gene trees with a species tree" << endl;
  cout << "For every unrooted gene tree and a species tree (details of costs):"
       << endl;

  cout << " o - print the optimal number of duplication and loss event (D,L)" << endl;
  cout << " m - print the optimal mutation cost" << endl;
  cout << " I - print all non-root-duplication rooted subtrees" << endl;
  cout << "      urec  -b -g '(((a,a),(b,c)),(b,a)) ' -s '(a,(b,c))' -I" << endl ;
  cout << " O - print one arbitrary optimal rooting of unrooted tree;" << endl;
  cout << "     here the rooting is placed in the middle of the rooting edge." << endl;
  cout << " P - show plateau size" << endl; // -a6

  cout << "Technical options" << endl;
  
  

  // OK  
  cout << endl
    << "For every species tree, i.e., summary of costs when reconciling a species tree with a set of gene trees):"
    << endl;        
  
  cout << " t - print total normalized cost; normalization by dupl. diameter |G|-1" << endl;  
  cout << "    urec -ut -g '((a,a),(b,c)); ((b,b),(a,c))' -s '(a,(b,c))'" << endl;
  cout << " c - print total mutation cost" << endl;
  cout << "    urec -uc -g '((a,a),(b,c)); ((b,b),(a,c))' -s '(a,(b,c))'" << endl;
  cout << " C - print total cost (D,L)" << endl;  
  cout << " d - print distributions of events per each subtree of the species tree" << endl;
  cout << " x - print event distributions embedded into the species tree (nested parenthesis notation with attributes)"
       << endl;

  
  cout << " Advanced options:" << endl;
  cout << "  -a5 - print candidates for episode rooting (up to 5 edges)" << endl;
  cout << "  -a51 - show the rooting on empty edge or nothing" << endl;
  cout << "  -a52 - print the rooting on double edge or max 2 candidates for empty edge gene trees" << endl;
  cout << "  -a53 - print all rootings with types: D-double, E-empty, L-leftempty, R-rightempty " << endl;
  cout << "  -a54 - print data for unrooted episode clustering" << endl;
  cout << "  -a2  - print cluster & split representation for DL-plateau" << endl; // -a2
  cout << "  -a24 - print cluster & split representation for D-plateau" << endl; // -a24

  cout << "  -a6:  print DL-plateau size" << endl; 
  cout << "  -a7:  print D-plateau size" << endl;  
  cout << "  -a4:  print plateau sizes for D and DL, and all rootings with event counts (weights ignored)" << endl; 
  cout << "  -a81: test whether there is an empty edge not-incident to a leaf " << endl; // -a81
  cout << "  -a1:  print attributes and mappings" << endl; 
  cout << "  -a3:  print detailed attributes" << endl; 
  cout << "  -an:  print all NNI neighbours; use -s/-S option for rooted or -g/-G for unrooted trees" << endl;
  cout << "     urec -N -s '(a,(b,c))'" << endl;
  cout << "     urec -N -g '(a,(b,c),d)'" << endl;


  cout << "Gene tree rootings: general and by unrooted reconciliation" << endl;
  cout << " -r [mroa1lewpxtc]+" << endl;
  
  cout << " [mro] - gene tree rootings set" << endl;
  cout << "   m: all midpoint rootings (default)" << endl;  
  cout << "   r: all rootings" << endl;  
  cout << "   o: rootings on the edges from the plateau" << endl;

  cout << " [as] - size of the rooting set" << endl;
  cout << "   a: all rootings satisfying conditions from r, m or o (default)" << endl;
  cout << "   s: show just one arbitrary rooting " << endl;

  cout << " [le] - using branch lengths" << endl;
  cout << "   l: determine midpoint rootings using branch lengths (default)" << endl;
  cout << "   e: determine midpoint rootings using edges, i.e., set lengths to 1" << endl;
  cout << "      output trees have no branch lengths printed" << endl;

  cout << " [wp] - using edges to determine midpoint rootings" << endl;  
  cout << "   w: from the whole tree (default)" << endl;  
  cout << "   p: from the plateau only, i.e., branch lengths outside plateau are set to 0" << endl;
   
  cout << "  x: print exteded info on rootings" << endl;
  cout << "  t: print gene tree root-split and edge types for each rooting" << endl;
  cout << "  i: info on the rooting algorithm ([w]hole, [p]lateau, [a]ll, [s]ingle, by[e]dges, by[l]enghts, [m]idpoint, all[r]ootings,) " << endl;

  cout << "  c: print edges (rootings) connectivity, i.e., for each edge its edge neighbours" << endl;  
    
  //cout << "  D - use D-optimal rootings; default cost is DL; only with the next suboptions" << endl; 

  
  cout << endl;
  cout << "Event weights" << endl;
  cout << " -D weight - set weight of gene duplications" << endl;
  cout << " -L weight - set weight of gene losses" << endl;


  cout << endl;
  cout << "Random Trees" << endl;
  cout << " -R labels[;g|u|s][[;]leafsetsize]] - random trees generator" << endl;
  cout << "   labels: a number, a tree or a char set e.g., abcdef" << endl;
  cout << "   g: gene tree generator (multilabeled)" << endl;
  cout << "   u: species tree generator using random sets of labels" << endl;
  cout << "   s: species tree generator using all labels" << endl;
  cout << "   leafsetsize: the number of leaves in generated trees" << endl;
  
  cout << " -N num0[;num1[;rnum2[;rnum3]]] - additional parameters for random generator" << endl;
  cout << "   num0 - the number of random trees (default 1)" << endl;
  cout << "   num1 - number of internal nodes (approx.)" << endl;
  cout << "   rnum2 - probability of an internal node" << endl;
  cout << "   rnum3 - decreased probability of an internal node" << endl;

  cout << endl;
  cout << "Matching gene and species labels" << endl;
  cout << " -m pNUM|p-NUM|aDELIM|bDELIM - mapping gene identifiers to species names" << endl
       << "    pNUM - species name in the first NUM characters of gene ids" << endl
       << "    p-NUM - species name in the last NUM characters of gene ids" << endl
       << "    aDELIM - species name after delimiter DELIM" << endl
       << "    bDELIM - species name before delimiter DELIM" 
       << endl;


  cout << endl; 
  cout << "Other commands" << endl;


  cout << " -Mg:  graphviz output for gene trees:" << endl;  
  cout << "     urec -Mg -g '((a1:1,a2:2):2,a3:2)' -p | neato -Tpdf > gtree.pdf" << endl; 
  cout << "     urec -Mg -g '((a1:1,a2:2):2,a3:2,a4:1)' -p | neato -Tpdf > gtree.pdf" << endl; 
  
  exit(-1);
}


int plateauborder(UNode *u, RNode *mg)
{
  if (u->leaf()) return 1;
  UNode3 *u3 = (UNode3*)u;
  return u3->l()->p()->lcaMapping(0) != mg && u3->r()->p()->lcaMapping(0) != mg;
}

int getsidedepth(UNode *u)
{
  if (u->leaf()) return 1;
  UNode3 *u3 = (UNode3*)u;  
  if (u3->l()->p()->depth && u3->r()->p()->depth)
    return 1 + max(u3->l()->p()->depth, u3->r()->p()->depth);
  return 0;
}

const char * nodetype(UNode *u)
{
  if (u->leaf()) return "L";
  UNode3 *u3 = (UNode3*)u;
  if (u3->l()->p()->lcaMapping(0) == u3->lcaMapping(0) 
    || u3->r()->p()->lcaMapping(0) == u3->lcaMapping(0))
    return "D";
  return "S";

}

UNode *check2epis(UNode* un, SpeciesTree *s, int optdup)
{
  if (un->leaf()) return NULL;

  UNode *r = ((UNode3*)un)->r()->p();
  DlCost dlu = r->cost(s);
  if (dlu.dup != optdup) return NULL;
  // in D-plateau
  UNode *l = ((UNode3*)un)->l()->p();
  //cout << *r->smprooted() << "----" << *l->smprooted() << endl;
  //cout << *r->M(s) << "----" << *l->M(s) << "     " << *un->M(s)   << endl;
  if (un->lcaMapping(s) == l->lcaMapping(s)) return l;
  return r;
}


int umetraverse(UNode* un, SpeciesTree *s, int optdup, int index)
{

  DlCost dlu = un->cost(s);
  if (dlu.dup != optdup) return 0;  

  // Print D-plateau rooting
  cout << index << " "; PROOTINGS(un); cout << endl;

  if (un->leaf()) return 1; // finished

  // traverse to next  
  umetraverse( ((UNode3*)un)->l()->p(), s, optdup, index);
  umetraverse( ((UNode3*)un)->r()->p(), s, optdup, index);
  return 1; // Non-empty 
}

void pinfo(UNode *u, RNode *mg, SpeciesTree *s)
{
  if (plateauborder(u, mg)) cout << "Bor ";
  else cout << "Plt ";
  cout << nodetype(u);
  cout << " " << *u->smprooted() << " " << *u->lcaMapping(s) << endl;
}


void pinfod(UNode *u, RNode *mg, SpeciesTree *s)
{
  if (u->leaf()) cout << "Bor ";
  else
  {
    DlCost d1 = u->cost(s);
    DlCost d2 = ((UNode3*)u)->r()->p()->cost(s);
    DlCost d3 = ((UNode3*)u)->l()->p()->cost(s);
    if ((d1.dup==d2.dup) && (d1.dup==d3.dup)) 
      cout << "Plt ";
    else cout << "Bor ";
  }
  //if (plateauborder(u, mg)) cout << "Bor ";
  //else cout << "Plt ";
  cout << nodetype(u);
  cout << " " << *u->smprooted() << " " << *u->lcaMapping(s) << endl;
}

void ppcluster(RNode *l)
{
  for (size_t id=0; id<specnames.size(); id++)
    if (l->hasspecies(id)) cout << specnames[id] << " ";
}

void ppsplit(RNode *l)
{
  if (l->leaf()) ppcluster(l);
  else
  {
    ppcluster(((RInt*)l)->l());
    cout << ": ";
    ppcluster(((RInt*)l)->r());
  }
}


void midpointrootings(int rootingtype, UTree *g, SpeciesTree *s=NULL)
{

  bool oldlenset[g->leaves()*4-6];  
  double oldlens[g->leaves()*4-6];  
  UNode *ur;
  double optcost=-100000;

  bool byedges = rootingtype & RT_BYEDGES;
  bool plateau = rootingtype & RT_BYPLATEAU;

  int rootingset = 0;
  bool extended = rootingtype & RT_EXTENDED;
  bool onerooting = rootingtype & RT_ONEROOTING; // or all
  
  if (rootingtype & RT_MIDPOINT) rootingset = RT_MIDPOINT;
  else if (rootingtype & RT_OPTROOTINGS) rootingset = RT_OPTROOTINGS;  
  else rootingset = RT_ALL;
 

  if (s) optcost=g->findoptimaledge(s)->cost(s).mut();
  
  g->storebranchlen(oldlenset,oldlens);
                      
  if (byedges && !plateau) //23
  {    
   // ignore branch lengths (set all to 1)      
    iterator_utree itu2(g);
    while ((ur = itu2()) != 0)              
      ur->setbranchlen(1);              
  }

  if (!byedges && plateau) //23
  // (rootingtype==MIDPOINTPLATEAUROOTINGSLENGTHS) 
  {          
    // plateaumidpoint
    iterator_utree itu2(g);
    while ((ur = itu2()) != 0)                      
      //if (dlu.dup != dl.dup || dlu.loss != dl.loss)     
      if (ur->cost(s).mut() != optcost)                                                   
        ur->setbranchlen(0);                                            
  }

  if (byedges && plateau) //23
    //(rootingtype==MIDPOINTPLATEAUROOTINGSEDGES) 
     
  {          
    iterator_utree itu2(g);
    while ((ur = itu2()) != 0)                      
      if (ur->cost(s).mut() == optcost)                                                   
        ur->setbranchlen(1);
      else ur->setbranchlen(0);                             
  }

  // set defaults if no branchlen is given
  iterator_utree itu3(g);
  while ((ur = itu3()) != 0)                      
    if (!ur->haslen()) 
      ur->setbranchlen(1);                             


  iterator_utree itu2(g);
  UNode* t[g->leaves()*2-3];  // edges num
  int edgecnt = 0;
  while ((ur = itu2()) != 0) {              
    if (ur->marked() & M_PROCESSED) continue;              
    ur->mark(M_PROCESSED);
    ur->p()->mark(M_PROCESSED);
    t[edgecnt++] = ur;
  } 

  double h[edgecnt],hl[edgecnt],hr[edgecnt];
  double minh=-1;  
  for (int i=0;i<edgecnt;i++)
  {
    double a=t[i]->subtreeheightbybranchlen();
    double b=t[i]->p()->subtreeheightbybranchlen();
    double c=t[i]->getbranchlen();
    double m=(a+b+c)*0.5;    

    if (m>=max(a,b)) { h[i]=m; hl[i]=m-a; hr[i]=m-b; }
    else if (m<b) { h[i]=b; hl[i]=c; hr[i]=0; }                 
    else { h[i]=a; hr[i]=c; hl[i]=0; }
                                        
    if (i) minh=min(minh,h[i]);
    else minh=h[i];              
  }

    
    //g->loadbranchlen(oldlenset,oldlens);  

  if (byedges)
  {
    // remove branchlengths
    iterator_utree itu(g);
    while ((ur = itu()) != 0)                          
      ur->unsetbranchlen();   
  }
  else 
     g->loadbranchlen(oldlenset,oldlens);  


    int cnt=0;

    RNode *MG = NULL;
            
    for (int i=0;i<edgecnt;i++)
    {
        
        
        if (rootingset==RT_MIDPOINT && h[i]!=minh) continue;
        if (rootingset==RT_OPTROOTINGS && s && (t[i]->cost(s).mut() != optcost)) continue;

        if (s && !MG)
          MG = s->lca(t[i]->lcaMapping(s), t[i]->p()->lcaMapping(s));

        if (extended)
        { 
          cout << "id=" << t[i]->getedgeid() << " ";                      
          cout << h[i] << " ";                
        }

        t[i]->ppmidheightrooting(cout,hl[i],hr[i]);        

        if (rootingtype & RT_ROOTSPLIT)
        { 
          UNode *A = t[i];
          UNode *B = t[i]->p();
          
          cout << " [ ";
          ppsplit(A->smprooted());          
          cout << "| ";        
          ppsplit(B->smprooted());          
          cout << "] "; 
        
          if (s)
          {
              if (MG->leaf()) cout << " D ";
              else                
              {
                bool mA = A->lcaMapping(s)==MG;
                bool mB = B->lcaMapping(s)==MG;
                if (mA && mB) cout << " D ";
                else if (!mA && !mB) cout << " E ";
                else if (mA && !mB) cout << " < ";
                else cout << " > ";
              }
          }
        }
        
        if (extended)
        {
          if (s && (t[i]->cost(s).mut() == optcost)) cout << " plateau";      
    
          if (h[i]==minh) 
          {                      
              cout << " midpoint"; // << rootingtype;            
          }

          if (t[i]->getstart() || t[i]->p()->getstart())
              cout << " start";
        }

        cnt++;
        cout << endl;   
        if (cnt==1 && onerooting) break;           
    }
        

          

    g->loadbranchlen(oldlenset,oldlens);                                
}


int main(int argc, char **argv) {
  int opt;
  int rt_len = 2;
  int rt_leafsetsize = -1;
  int randomtreescnt = 1;
  double rt_pint = 0.5;
  double rt_dec = 0.75;
 
  if (argc < 2)
    usage(argc, argv);
  vector<SpeciesTree*> stvec;
  vector<UTree*> gtvec;
  //srand(time(0));

  struct timeval t1;
  gettimeofday(&t1, NULL);  
  srand(t1.tv_usec * t1.tv_sec);

  int genopt = 0;
  
  vector<string> genrandtreesstr;
  bool genrandtrees = false; 

  bool adv_options = false;
  bool adv_dlplateausize = false;
  bool adv_plateausize = false;
  bool adv_dplateausize = false;
  bool adv_splitclusterrepr = false;
  bool adv_splitclusterreprD = false;
  bool adv_internalemptyedgetest = false;
  bool adv_alloptrootings = false;

  bool adv_epicand5 = false;  // OK
  bool adv_epiemptyedge51 = false; // O K
  bool adv_epidouble52 = false; // OK
  bool adv_epirootingtypes53 = false;  // OK
  bool adv_epirootingtypes54 = false;  // OK
  
  bool adv_optclusters4 = false;
  int rootingtype = 0;
  
  while ((opt = getopt(argc, argv, "g:s:t:G:S:T:pPvN:R:n:u:r:a:m:L:D:M:"))
         != -1)
    switch (opt) 
    {
      case 'g':
        readtrees(optarg, &gtvec, NULL, 1);         
        break;
      case 's':
        readtrees(optarg, NULL, &stvec, 1);               
        break;
      case 't':
        readtrees(optarg, &gtvec, &stvec, 1);
        break;
      case 'S':
        readtrees(readfile(optarg), NULL, &stvec);
        break;
      case 'G':
        readtrees(readfile(optarg), &gtvec, NULL);
        break;
      case 'T':
        readtrees(readfile(optarg), &gtvec, &stvec);      
        break;
   
    case 'p':
      genopt |= OPT_PRINTGENE;
      break;

    case 'P':
      genopt |= OPT_PRINTSPECIES;
      break;


    // weights
    case 'L':
      if (sscanf(optarg, "%lf", &weight_loss) != 1) {
        cerr << "Number expected in -L" << endl;
        exit(-1);
      }
      break;

    case 'D':
      if (sscanf(optarg, "%lf", &weight_dup) != 1) {
        cerr << "Number expected in -D" << endl;
        exit(-1);
      }
      break;
          
    // Rand trees:  leafsetspec;g|u|s;leavessize
    case 'R':
    {
      genrandtrees = true;
    
      char *p = strchr(optarg,';');
      if (p) 
      {
        p[0]=0;
        p++;  
      }
      genrandtreesstr = initgenrand(optarg);

      if (p)
      {
          if (p[0]=='u') genopt |= OPT_RANDUNIQUE;
          else if (p[0]=='s') 
          {
            genopt |= OPT_RANDUNIQUE;
            rt_leafsetsize = genrandtreesstr.size();
          }
          else if (p[0]!='g')
          {
            cerr << "Expected u,s or g in -r spec" << endl;
            exit(-1);
          }
          p++;
          if (!*p) break;
          if (p[0]==';') p++;
          if (!*p) break;
          if (sscanf(p, "%d", &rt_leafsetsize) != 1) {
            cerr << "Number expected in last value of -r " << endl;
            exit(-1);
          }
      }
          
      break;
    }

    case 'N': //"#,n;i;e"

      if (sscanf(optarg, "%d;%d;%lf;%lf", &randomtreescnt, 
        &rt_len,&rt_pint,&rt_dec)<1)
      {
        cerr << "At least one number expected in -n" << endl;
        exit(-1);
      }
      break;

    // unrooted reconciliation

    case 'u':
      genopt |= OPT_BYCOST;
      
      if (strchr(optarg,'o')) genopt |= OPT_RECMINCOST; //o 
      if (strchr(optarg,'m')) genopt |= OPT_RECMUTCOST; // 
      if (strchr(optarg,'O')) genopt |= OPT_RECMINROOTING; // O
      if (strchr(optarg,'I')) genopt |= OPT_RECMINROOTINGEXT; // I
      if (strchr(optarg,'c')) genopt |= OPT_SUMMARYTOTAL; // c
      if (strchr(optarg,'t')) normalizecost = 1; // t
      if (strchr(optarg,'C')) genopt |= OPT_SUMMARYDLTOTAL; // C
      if (strchr(optarg,'d')) genopt |= OPT_SUMMARYDISTRIBUTIONS; 
      if (strchr(optarg,'x')) genopt |= OPT_TREEDISTRIBUTIONS;
      if (strchr(optarg,'P')) adv_options = adv_plateausize = true; // plateau size general

      

      // Hidden      
      if (strchr(optarg,'X')) genopt |= OPT_RECTREECOSTDETAILS; /// ???     
      

      break;

    case 'r':
      
      //if (strchr(optarg,'a')) adv_options = adv_alloptrootings = true; // -a8

      //midpoint x plateau|whole x lengths|edges x all|one
      //rootings x lengths|edges x all|one
      //optrootings x lengths|edges x all|one


      // Use w - whole tree (def) OR p - platueau to determine rootings
      // l - branch lengths (def) OR e - by edges
      // m - midpoint (def) OR r - rootings  OR o - optrootings
      // a - all variants OR 1 - one arbitrary single rooting


      if (strchr(optarg,'O')) genopt |= OPT_RECMINROOTING; // -O start

          

      if (strchr(optarg,'p')) rootingtype |= RT_BYPLATEAU; // compl. is whole tree
      
      if (strchr(optarg,'e')) rootingtype |= RT_BYEDGES;   // def -lengths
      
      if (strchr(optarg,'r')) rootingtype |= RT_ALL;  
      if (strchr(optarg,'m')) rootingtype |= RT_MIDPOINT;  // def.
      if (strchr(optarg,'o')) rootingtype |= RT_OPTROOTINGS;  

      if (strchr(optarg,'s')) rootingtype |= RT_ONEROOTING;      

      if (strchr(optarg,'x')) rootingtype |= RT_EXTENDED;
      if (strchr(optarg,'t')) rootingtype |= RT_ROOTSPLIT;

      
      if (!(rootingtype & (RT_ALL|RT_OPTROOTINGS))) rootingtype|=RT_MIDPOINT;
      
      if (rootingtype & (RT_OPTROOTINGS|RT_BYPLATEAU)) genopt |= OPT_BYCOST;        

      if (strchr(optarg,'c')) rootingtype |= RT_CONNECTIVITY;



      if (strchr(optarg,'i'))
      { 
        
        if (rootingtype & RT_BYEDGES) cout << "byedges" << " "; else cout << "bylengths" << " ";
        if (rootingtype & RT_BYPLATEAU) cout << "plateau" << " "; else cout << "whole" << " ";        
        if (rootingtype & RT_ONEROOTING) cout << "single" << " ";
        if (rootingtype & RT_MIDPOINT) cout << "midpoint" << " ";
        if (rootingtype & RT_OPTROOTINGS) cout << "optrootings" << " ";              
        if (rootingtype & RT_ALL) cout << "allrootings" << " ";        
        cout << endl;
      }
      
    break;
    

    case 'a':
    {      


      if (!strcmp(optarg,"n"))      
      {
        genopt |= OPT_NNI;
        break;
      }

      int atype = 1;
      
      if (sscanf(optarg, "%d", &atype) != 1) {
        cerr << "Number expected in -a" << endl;
        exit(-1);
      }


   // -o - show an optimal cost
   // -O - show an optimal rooting
   // -I - show all non-root-duplication rooted subtrees
   // -a1 - show attributes and mappings
   // -
   // a2 - show cluster & split representation
   // -a21 - plateau median tree
   // -a22 - midpoint rootings by branch lengths
   // -a23 - midpoint rootings by branches
   // -a24 - cluster representation for D-plateau
   // -a3 - show detailed attributes
   // -a4 - show all rootings with costs
   // -a8 - show all dl-optimal rootings
   // -a9 - show all d-optimal rootings
   // -a81 - empty edge not-incident to a leaf 

   // -a51 - show the rooting on empty edge or nothing
   // -a52 - show the rooting on double edge or max 2 candidates for empty edge gene trees
   // -a53 - show all rootings with types: D-double, E-empty, L-leftempty, R-rightempty 
   // -a6 - show dl-plateau size
   // -a7 - show d-plateau size
   // -a5 - show candidates for episode rooting (up to 5 edges)
       
       switch (atype)
       {
        // verified
        case 5:  adv_epicand5 = true;  break;  
        case 51: adv_epiemptyedge51 = true; break;
        case 52: adv_epidouble52 = true; break;
        case 53: adv_epirootingtypes53 = true; break;
        case 54: adv_epirootingtypes54 = true; break;
        case 2:  adv_options = adv_splitclusterrepr = true; break; // a2
        case 24: adv_options = adv_splitclusterreprD = true; break; // a24
        case 81: adv_internalemptyedgetest = true; break;
        case 1: genopt |= OPT_RECINFO; break;
        case 3: genopt |= OPT_RECINFO | OPT_RECDETAILS; break;

        // OK
        case 6: adv_options = adv_dlplateausize = true; break; // DL-plateau size
        case 7: adv_options = adv_dplateausize  = true; break; // D-plateau size 
        case 4: adv_options = adv_optclusters4 = true; break;

        // case 8: adv_options = adv_dlplateausize = true; break; // DL-plateau rootings
        // case 9: adv_options = adv_dplateausize  = true; break; // D-plateau rootings

        // not verified
        
        
        default:
        cerr << "Invalid code for -a option" << endl;
        exit(-1);

      }
      genopt |= OPT_BYCOST;
      adv_options = true;
      break;
    }
     
    // Matching OK
    case 'm':
      switch (optarg[0])
      {
      case 'p':
        gspos = atoi(optarg + 1);
        gsid = GSPOS;
        break;
      case 'a':
        gsid = GSAFTER;
        gsdelim = strdup(optarg + 1);
        break;
      case 'b':
        gsid = GSBEFORE;
        gsdelim = strdup(optarg + 1);
        break;
      default:
        cerr << "Invalid code for -m option expected aDELIM,bDELIM or p[-]number" << endl;
        exit(-1);
      }
      break;

    case 'M':      
        int j;
        for (j=0;j<(int)strlen(optarg);j++)
        {
        // TODO: CHECK
          if (optarg[j]=='c') gcenter=1;
          if (optarg[j]=='v') gcenter=1;
          if (optarg[j]=='s') rsort=1;
          if (optarg[j]=='g') ppgraphviz=1;
          if (optarg[j]=='i') printedgeids=true;
          if (optarg[j]=='l') printleaves=1;
          if (optarg[j]=='S') {
            int i;
            for (i=0; i<(int)specnames.size(); i++)
              cout << i << ". " << specnames[i] << endl;
          }
        }

      break;


      case 'v':
      genopt |= OPT_VOTING;
      break;

    default:
      cerr << "Unknown option: " << ((char) opt) << endl;
      exit(-1);
    }


  // Rand trees generator
  if (genrandtrees)
  {
      int i;
      if (rt_leafsetsize == 0)
        rt_leafsetsize = specnames.size();
      for (i = 0; i < randomtreescnt; i++)
        gtvec.push_back(new UTree(rt_len, rt_pint, rt_dec, rt_leafsetsize, (genopt & OPT_RANDUNIQUE),  genrandtreesstr ));
  }    

  // Extra arguments - gene trees
  for (; optind < argc; optind++) 
    readgtrees(readfile(argv[optind++]), gtvec);  

  vector<SpeciesTree*>::iterator stpos;
  vector<UTree*>::iterator gtpos;

  if (genopt & OPT_PRINTGENE)
  {
    for (gtpos = gtvec.begin(); gtpos != gtvec.end(); ++gtpos)
    {
      if (gcenter) (*gtpos)->center();

      if (rsort) 
        (*gtpos)->normalize()->printsorted(cout, 0) << endl;
      else 
        if (ppgraphviz) 
          (*gtpos)->printgraphviz(cout);
      else        
          (*gtpos)->print(cout) << endl;          
        
    }
  }


  if (genopt & OPT_NNI)
  {
    // NNI operations for the set of gene/species trees
    nnist(stvec);
    nnigt(gtvec);
  }

  if (genopt & OPT_PRINTSPECIES) {
    for (stpos = stvec.begin(); stpos != stvec.end(); ++stpos)
      (*stpos)->print(cout) << endl;
  }

  if (genopt & OPT_PRINTROOTINGS) {
    for (gtpos = gtvec.begin(); gtpos != gtvec.end(); ++gtpos)
      (*gtpos)->printrootings(cout);
  }

  if (genopt & OPT_VOTING) {

    int trnum = stvec.size();
    double mincnts[trnum];
    int i;

    for (i = 0; i < trnum; i++)
      mincnts[i] = 0;

    int j = 0;
    for (gtpos = gtvec.begin(); gtpos != gtvec.end(); ++gtpos) {
      j++;
      double min = 0;
      int minc = 0;
      UTree *g = *gtpos;
      i = 0;
      for (stpos = stvec.begin(); stpos != stvec.end(); ++stpos) {
        g->clear();
        UNode *un = g->findoptimaledge(*stpos);
        double m = (un->cost(*stpos)).mut();
        if (i == 0) {
          min = m;
          minc = 1;
        } else if (min > m) {
          min = m;
          minc = 1;
        } else if (min == m)
          minc++;
        i++;
      }
      i = 0;
      for (stpos = stvec.begin(); stpos != stvec.end(); ++stpos) {
        g->clear();
        UNode *un = g->findoptimaledge(*stpos);
        if ((un->cost(*stpos)).mut() == min)
          mincnts[i] += 1.0 / minc;
        i++;
      }

    }
    i = 0;
    for (stpos = stvec.begin(); stpos != stvec.end(); ++stpos)
      cout << **stpos << " " << mincnts[i++] << endl;
  }

  if (rootingtype & RT_CONNECTIVITY)
  {
      for (gtpos = gtvec.begin(); gtpos != gtvec.end(); ++gtpos) 
      {
        UTree *g = *gtpos;
        g->clear();        
        g->connectivity(cout);
      }
      rootingtype&=~RT_CONNECTIVITY;
      exit(0);
  }

  if (rootingtype)
  {
      if (!(rootingtype & (RT_BYPLATEAU|RT_OPTROOTINGS)))
      {
        for (gtpos = gtvec.begin(); gtpos != gtvec.end(); ++gtpos) 
        {
            UTree *g = *gtpos;
            g->clear();
            midpointrootings(rootingtype,g,NULL);  
        } 
      }
      else
      {
        if (!stvec.size())
        {
          cerr << "No species tree is defined. Cannot root a gene tree..." << endl;
          exit(-1);
        }        
      }
  }

  if (genopt & OPT_BYCOST) 
  {
    
    for (stpos = stvec.begin(); stpos != stvec.end(); ++stpos) 
    {

      SpeciesTree *s = *stpos;
      if (genopt & OPT_RECINFO)
        cout << "Species tree;" << *s << endl;

      DlCost total;

      double normcost=0;
      for (gtpos = gtvec.begin(); gtpos != gtvec.end(); ++gtpos) 
      {
        UTree *g = *gtpos;
        g->clear();


        if (genopt & OPT_RECINFO) 
        {
          cout << "Gene tree;";
          g->print(cout) << endl;

          iterator_utree itu(g);
          UNode *ur;
          while ((ur = itu()) != 0) {
            if (ur->leaf())
              cout << "** leaf " << ((ULeaf*) ur)->species();
            else {
              cout << "** int  ";
              if (genopt & OPT_RECDETAILS)
                cout << "  " << *ur->smprooted() << endl;
            }
            if (genopt & OPT_RECDETAILS)
              cout << "  p=" << *ur->p()->smprooted() << endl;
            cout << "\t sc=" << ur->sc(s);
            cout << "\t events=" << ur->cost(s) << "\t ";
            cout << *ur->smprooted() << " ==> " << *ur->lcaMapping(s)
                 << endl;
          }
        }
        UNode *un = NULL;

        if (rootingtype & (RT_BYPLATEAU|RT_OPTROOTINGS))
            midpointrootings(rootingtype,g,s);        



        if (adv_options)
        {

          iterator_utree itu(g);
          UNode *ur, *urp;
          un = g->findoptimaledge(s);
          UNode *unp = un->p();
          DlCost dl = un->cost(s);
          int dlplateausize = 0;
          int dplateausize = 0;
          int plateausize = 0;
          while ((ur = itu()) != 0) {
            DlCost dlu = ur->cost(s);
            if (dlu.dup == dl.dup && dlu.loss == dl.loss) dlplateausize++;
            if (dlu.dup == dl.dup) dplateausize++;
            if (dlu.mut() == dl.mut()) plateausize++; 
          }
          dlplateausize /= 2;
          dplateausize /= 2;
          plateausize /= 2;

          RNode *MG = s->lca(un->lcaMapping(s), un->p()->lcaMapping(s)); // M(G)

          if (adv_dlplateausize)
            cout << dlplateausize << endl;

          if (adv_plateausize)
            cout << plateausize << endl;
          
          if (adv_dplateausize)
            cout << dplateausize << endl;
          
          if (adv_internalemptyedgetest)
          {
            if (dlplateausize == 1 && un->lcaMapping(s) != MG && !un->leaf() && !unp->leaf())
              cout << "Internal empty edge" << endl;
          }
          
          if (adv_epiemptyedge51) // OK
          {
            if (dlplateausize == 1 && un->lcaMapping(s) != MG)
              PROOTINGS(un) << endl;
          }
          
          if (adv_epidouble52) // OK
          {
            // EPIS gen
            if (dlplateausize == 1 && un->lcaMapping(s) != MG)
            { // EMPTY EDGE
              // Find other connected
              ur = check2epis(un, s, dl.dup);
              if (ur) PROOTINGS(ur) << endl;

              ur = check2epis(unp, s, dl.dup);
              if (ur) PROOTINGS(ur) << endl;

            }
            else {
              PROOTINGS(un) << endl;
            }
          }

          if (adv_epirootingtypes54) // OK
          {
            // UME gen
            if (dlplateausize == 1 && un->lcaMapping(s) != MG)
            { // EMPTY EDGE
              cout << "E ";
              PROOTINGS(un) << endl;
              // Find other connected

              int start=1;
              for (int i=0;i<2;i++)
              {
                if (!un->leaf())
                  {
                    if (umetraverse(((UNode3*)un)->r()->p(), s, dl.dup,start)) start++;
                    if (umetraverse(((UNode3*)un)->l()->p(), s, dl.dup,start)) start++;                    
                  }
                un=unp;
              }
              

            }
            else {
              // Double edge present              
              iterator_utree itu2(g);
              while ((ur = itu2()) != 0) 
              {
                DlCost dlu = ur->cost(s);
                if (ur->marked() & M_PROCESSED) continue;
                if (dlu.dup == dl.dup)
                {
                  ur->mark(M_PROCESSED);
                  ur->p()->mark(M_PROCESSED);
                  cout << "D "; PROOTINGS(ur) << endl;
                }
              } //while                  
            }
          } 

          if (adv_epirootingtypes53) // OK
          {
            // EPIS gen
            if (dlplateausize == 1 && un->lcaMapping(s) != MG)
            { // EMPTY EDGE
              cout << "E ";
              PROOTINGS(un) << endl;
              // Find other connected

              ur = check2epis(un, s, dl.dup);
              if (ur) { cout << "L "; PROOTINGS(ur) << endl; }

              ur = check2epis(unp, s, dl.dup);
              if (ur) { cout << "R "; PROOTINGS(ur) << endl; }

            }
            else {
              cout << "D "; PROOTINGS(un) << endl;
            }

          }

          if (adv_epicand5) // OK
          {
            // EPIS gen
            if (dlplateausize == 1 && un->lcaMapping(s) != MG)
            { // EMPTY EDGE
              // Find other connected
              iterator_utree itu2(g);
              while ((ur = itu2()) != 0) {
                DlCost dlu = ur->cost(s);
                if (ur->marked() & M_PROCESSED) continue;
                if (dlu.dup != dl.dup) continue;
                urp = ur->p();
                if (adjacent(ur, un) || adjacent(urp, un) || adjacent(ur, unp) || adjacent(urp, unp))
                {
                  PROOTINGS(ur) << endl;
                  //PROOTINGSCOST(ur,dlu);
                }
                ur->mark(M_PROCESSED);
                ur->p()->mark(M_PROCESSED);
              }
            }
            else {
              //PROOTINGSCOST(un,dl);
              PROOTINGS(un) << endl;
            }

          }

          if (adv_optclusters4)
          {
            cout << "GENE TREE: " << endl;
            cout << "Optimal cost: " << dl << endl;
            cout << "DL-plateau size: " << dlplateausize << endl;
            cout << "D-plateau size: " << dplateausize << endl;

            // By edges

            iterator_utree itu2(g);
            while ((ur = itu2()) != 0) {
              DlCost dlu = ur->cost(s);
              if (ur->marked() & M_PROCESSED) continue;
              PROOTINGSCOST(ur, dlu);
              ur->mark(M_PROCESSED);
              ur->p()->mark(M_PROCESSED);
              //pinfo(ur,MG,s);
              //pinfo(ur->p(),MG,s);
            }
          }
          
          if (adv_alloptrootings)
          {
            // opt. dl-rootings
            iterator_utree itu2(g);
            while ((ur = itu2()) != 0) {
              DlCost dlu = ur->cost(s);
              if (ur->marked() & M_PROCESSED) continue;
              if (dlu.mut()==dl.mut())
              //if (dlu.dup == dl.dup && dlu.loss == dl.loss)
              {
                ur->mark(M_PROCESSED);
                ur->p()->mark(M_PROCESSED);
                PROOTINGS(ur) << endl;
              }
            }
          }

          
          if (adv_splitclusterrepr)
          {
            cout << "GENE TREE: " << endl;
            cout << "Optimal cost: " << dl << endl;
            cout << "Plateau size: " << dlplateausize << endl;

            if (dlplateausize == 1 && un->lcaMapping(s) != MG) cout << "Root S";
            else cout << "Root D";
            cout << " (" << *un->smprooted() << "," << *un->p()->smprooted() << ")"
                 << " " << *MG << endl;

            iterator_utree itu2(g);
            while ((ur = itu2()) != 0) {
              DlCost dlu = ur->cost(s);
              if (ur->marked() & M_PROCESSED) continue;
              if (dlu.dup == dl.dup && dlu.loss == dl.loss)
              {
                ur->mark(M_PROCESSED);
                ur->p()->mark(M_PROCESSED);
                pinfo(ur, MG, s);
                pinfo(ur->p(), MG, s);      }
            }

            iterator_utree itu3(g);
            while ((ur = itu3()) != 0) {
              DlCost dlu = ur->cost(s);
              if (ur->marked() & M_PROCESSED) continue;
              if (dlu.dup != dl.dup || dlu.loss != dl.loss)
              {
                if (ur->lcaMapping(s) != MG)
                {
                  cout << "Ext " << nodetype(ur) << " ";
                  cout << *ur->smprooted() << " " << *ur->lcaMapping(s) << endl;
                }
              }
            }
          }
          
          if (adv_splitclusterreprD)
          {
            cout << "GENE TREE: " << endl;
            cout << "Optimal cost: " << dl << endl;
            cout << "Plateau size: " << dlplateausize << endl;

            if (dlplateausize == 1 && un->lcaMapping(s) != MG) cout << "Root S";
            else cout << "Root D";
            cout << " (" << *un->smprooted() << "," << *un->p()->smprooted() << ")"
                 << " " << *MG << endl;

            iterator_utree itu2(g);
            while ((ur = itu2()) != 0) {
              DlCost dlu = ur->cost(s);
              if (ur->marked() & M_PROCESSED) continue;
              if (dlu.dup == dl.dup)
              {                
                ur->mark(M_PROCESSED);
                ur->p()->mark(M_PROCESSED);
                pinfod(ur, MG, s);
                pinfod(ur->p(), MG, s);      }
            }

            iterator_utree itu3(g);
            while ((ur = itu3()) != 0) {
              DlCost dlu = ur->cost(s);
              if (ur->marked() & M_PROCESSED) continue;
              if (dlu.dup != dl.dup)
              {
                if (ur->lcaMapping(s) != MG)
                {
                  cout << "Ext " << nodetype(ur) << " ";
                  cout << *ur->smprooted() << " " << *ur->lcaMapping(s) << endl;
                }
              }
            }
          }
        } // if adv_options


        if (normalizecost || genopt & (OPT_RECMINROOTING | OPT_RECMINROOTINGEXT 
            | OPT_RECMINCOST | OPT_RECMUTCOST
            | OPT_RECTREECOSTDETAILS | OPT_SUMMARYTOTAL
            | OPT_SUMMARYDLTOTAL | OPT_SUMMARYDISTRIBUTIONS
            | OPT_TREEDISTRIBUTIONS))
          un = g->findoptimaledge(s);

        if (genopt & OPT_RECMINROOTING)        // -u O
            un->printrooting(cout, 1);
        
        if (genopt & OPT_RECMINROOTINGEXT)   // -u I
            un->printrootingext(cout, s->root(), 1);
        
        if (genopt & OPT_RECMINCOST)  // -u o
            cout << un->cost(s) << endl;

        if (genopt & OPT_RECMUTCOST) // -u m
            cout << un->cost(s).mut() << endl;


        if (genopt & OPT_RECTREECOSTDETAILS) 
        {
          if (un->p())
            un->p()->mark(2 | 8);
          un->mark(2);
          g->pf(cout, s);
          un->pcosts(cout, un->cost(s), s);
        }

        if ((genopt & OPT_SUMMARYTOTAL) // -uc
            || (genopt & OPT_SUMMARYDLTOTAL)) { // -uC
          DlCost s1 = un->cost(s);
          total.loss += s1.loss;
          total.dup += s1.dup;
        }

        if (normalizecost) // -ut
        {
          if (g->leaves()==1) cout << "Small tree?" << endl;

          normcost+=un->cost(s).dup/(1.0*(g->leaves()-1));
        }

        if ((genopt & OPT_SUMMARYDISTRIBUTIONS) || (genopt    
            & OPT_TREEDISTRIBUTIONS))  // -uxd
          un->costdet(s);     

      }  // for gtpos 

      int nc=0;
      if (normalizecost || (genopt & (OPT_SUMMARYTOTAL | OPT_SUMMARYDLTOTAL
                    | OPT_SUMMARYDISTRIBUTIONS)))  //-uxd
      {
        nc=1;
        cout << *s << "\t";
      }

      if (normalizecost)  // -ut
        cout << normcost << "\t";

      if (genopt & OPT_SUMMARYTOTAL) // -uc
        cout << total.mut() << "\t";

      if (genopt & OPT_SUMMARYDLTOTAL) // -uC
        cout << total << "\t";

      if (nc) cout << endl;

      if (genopt & OPT_SUMMARYDISTRIBUTIONS) // -ud
        s->showcostdet(cout); 
      
      if (genopt & OPT_TREEDISTRIBUTIONS)  //-ux
        s->pfcostdet(cout);

    } // st-randomtreescnt
  } // (OPT_BYCOST)

}
