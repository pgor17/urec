Urec is a tool written in C++ to perform unrooted reconciliation.

To compile from sources just type: "make".

To run and get help type: "urec".

### Cite

Pawel Gorecki and Jerzy Tiuryn, URec: a system for unrooted reconciliation, Bioinformatics 2007 23(4):511-512

Pawel Gorecki and Jerzy Tiuryn, Inferring phylogeny from whole genomes, Bioinformatics 2007 23: e116-e122 (Proc. of ECCB 2006)

### Usage

    ./urec [options] [-g GENE(S)TREESTR] [-s SPECIESTREE(S)STR] [[-G] FILE_GENETREES] [-S FILE_SPECIESTREES] ... ]


### Typical usage examples

Print one rooting with 
the optimal weighted duplication-loss cost (-rmp1), called mutation cost, and the cost (-um). 
The rooting 
is obtained by the midpoint of the subtree, called plateau, consisting of edges with the optimal cost. This strategy is called midpoint-plateau rooting.

```
> ./urec -G data/g3smp.txt -S data/g3phylogeny.txt -um -rmp1
(YALI:0.1561,(DEHA:0.1047,(((ERGO:0.09967,KLTH:0.08796):0.02058,(KLLA:0.07922,SAKL:0.04347):0.0132):0.02029,((ZYRO:0.10383,(SACE:0.0462,SACE:0.05212):0.04795):0.02109,CAGL:0.05978):0.01504):0.09161):0.1561)
10
(YALI:0.56248,(DEHA:0.63612,((SACE:1.56599,(KLLA:0.33705,(ERGO:0.4467,(KLTH:0.33421,SAKL:0.23057):0.09173):0.03978):0.07978):0.08548,(CAGL:0.69852,(ZYRO:0.46287,SACE:0.31986):0.08152):0.06064):0.61477):0.56248)
12
((ERGO:0.61312,((YALI:0.98503,DEHA:0.9226):0.48054,(KLLA:0.61842,(KLTH:0.5953,SAKL:0.34817):0.11598):0.0001):0.10347):0.139465,(ZYRO:0.66194,((SACE:0.56865,CAGL:0.77682):0.05986,YALI:1.47975):0.09807):0.090645)
21
```

### Tree format

Urec reads trees using newick notation with (optional) branch lenghts. 
Collections of trees can be either EOLN- or semicolon-separator.

```
> cat data/utrees.txt 
(a,(b,c));
((a,b),c);
(a,b,c);
(a:3,(b:2,c:13):6);
(a:9,b:2,c:13);
(a:1,b:2);
```

### Reading trees: a string, a file or a stdin.

 `-s STR`: species trees from a string, semicolon separated

 `-g STR`: unrooted gene trees from a string, semicolon separated

 `-t STR`: trees in interleaved format: gene_tree <EOLN or ;> species_tree <EOLN or ;> ...  

 `-S FILE`: similar to -s, but from a file

 `-G FILE`: similar to -g, but from a file

 `-T FILE`: similar to -t, but from a file

 `-p`: print gene tree(s)

 `-P`: print species tree(s)
```

> ./urec  -g "((a1:2,a2:3):1,a3:7,a4:2); (a:2,a:3); (a:2,(a:1,b:3):1)" -p
((a3:7,a4:2):1,a1:2,a2:3)
(a:2,a:3)
(a:3,a:1,b:3)
```

Alternatively gene tree files can be defined by extra arguments. Below an example of parsing and printing gene trees (-p).

```
> ./urec data/utrees.txt -p	
(a,b,c)
(c,a,b)
(a,b,c)
(a:9,b:2,c:13)
(a:9,b:2,c:13)
(a:1,b:2)
```

Use `-` instead of FILE to read from stdin.

```
> echo "(a,b,c)" | ./urec - -p
(a,b,c)
```

Print a species tree:
```
> ./urec -s	'(x,(y,z))' -P
(x,(y,z))
```

Print gene tree, interleaved format.
```
> ./urec -t "(a,b,a);(a,(b,c))" -p
(a,b,a)

> ./urec -t "(a,b,a);(a,(b,c));(c,(c,(c,c)))" -p
(a,b,a)
(c,c,(c,c))
```

### Rooting a gene tree (-r and suboptions).

#### Printing all rootings (-rr) and general rules.

Note, that every rooting in urec is placed on an edge. 

A tree without branch lenghts:
```
> ./urec  -g "((a,b),c,d)" -rr
((c,d),(a,b))
(c,(d,(a,b)))
(d,((a,b),c))
(b,((c,d),a))
(a,(b,(c,d)))
```

When using branch lengths, the root position minimizes the height. Thus, in some cases the inferred branch  length of some edges adjacent to the root may be 0.
When using branch lengths, the root position minimizes the height.

```
> ./urec -g "(a:2,b:2,c:2)" -rr
(a:2,(b:2,c:2):0)
(b:2,(c:2,a:2):0)
((a:2,b:2):0,c:2)

./urec -g "(a:1,b:2)" -rr # here the distance between a and b is 3.
(a:1.5,b:1.5)
```

Use suboption `x` to print extended info on rootings in the form: 
```id=<ROOTINGID> optimalheight rooting [midpoint] [plateau] [start]```

The `plateau` will be present only if the species tree is provided,
while `start` indicates the binary start, i.e., the original root, node only 
if the gene tree is of the form `(X,Y)` (not `(X,Y,Z)`).

Checking which suboptions are set: use suboption `i`.

### A classical midpoint rooting by branch lengths, default (-rm); no species tree is required

```
> ./urec  -g "(a1:2,a2:3,a3:4)"  -rm
((a1:2,a2:3):0.5,a3:3.5)
```

As output trees are binary, a midpoint rooting can be ambigouous; this is rare with the empricical data:
```
> ./urec -g "((a1:2,a2:3):1,a3:4,b:2)"  -rm
((a1:2,a2:3):1,(a3:4,b:2):0)
(a3:4,(b:2,(a1:2,a2:3):1):0)
(((a1:2,a2:3):1,a3:4):0,b:2)
```

Use suboption `s` (i.e., `-rms` or `-rs`), to print just one arbitrary midpoint rooting.
```
> ./urec -g "((a1:2,a2:3):1,a3:4,b:2)"  -rs
((a1:2,a2:3):1,(a3:4,b:2):0)
```

A full example with the extended output:
```
> ./urec -g "((a1:2,a2:3):1,a3:4,b:2)"  -rmx
id=2 4 ((a1:2,a2:3):1,(a3:4,b:2):0) midpoint
id=5 4 (a3:4,(b:2,(a1:2,a2:3):1):0) midpoint
id=6 4 (((a1:2,a2:3):1,a3:4):0,b:2) midpoint
```

All rootings with plateau info; start is shown if the gene tree has a binary start node.
```
> ./urec -m p1 -g "((a1,a2),(a3,b))" -s "(a,b)"  -rrxp 
id=0 1 (a1,(a2,(a3,b)))
id=1 1 (a2,((a3,b),a1))
id=2 1 ((a1,a2),(a3,b)) start
id=5 1 (a3,(b,(a1,a2)))
id=6 0.5 (b,((a1,a2),a3)) plateau midpoint
```

Print a gene tree with edge identifiers.
```
> ./urec -g "((a1,a2),(a3,b))" -Mi -p
((a3[id=5],b[id=6])[id=2],a1[id=0],a2[id=1])

> ./urec -g "(a,b,c);(a,b);(a,(b,c),d)" -Mi -p
(a[id=0],b[id=1],c[id=2])
(a[id=6],b[id=6])
(a[id=8],(b[id=9],c[id=10])[id=11],d[id=14])
```

### Midpoint rootings by edges 

Here, the rules are similar to the above example, but the midpoint rooting(s)
are computed under the assumptiom that all branch lengths are set to 1.0. The output will contain tree(s) without branch lengths.

A simple example:
```
./urec -g "(a,b,(c,d))"  -re
((a,b),(c,d))
```

More than one edge-midpoint rooting is also possible, and it may occur more often that previously:
```
./urec -g "(a,b,c)"  -re
(a,(b,c))
(b,(c,a))
((a,b),c)
```

Using suboptions `s` (a single arbitrary tree):
```
./urec -g "(a,b,c)"  -res
(a,(b,c))
```

Branch lenghts are ignored:
```
./urec -g "(a:2,b:4,c:2)"  -rme
(a,(b,c))
(b,(c,a))
((a,b),c)
```

### -a20 Midpoint plateau rootings by plateau branch lengths (i.e., midpoint rooting under the assumption that non-plateau lengths are set to 0)

### -a21 Midpoint plateau rootings by edges (branch lengths are not used)




### Printing more info on rootings (suboption x)



### Random trees (-R, -N)

`-R labels[;g|u|s[[;]leafsetsize]]]`: random trees generator
   `labels`: a number, a tree or a char set e.g., abcdef
   `g`: gene tree generator (multilabeled)
   `u`: species tree generator using random sets of labels
   `s`: species tree generator using all labels
   `leafsetsize`: the number of leaves in generated trees

`-N num0[;num1[;rnum2[;rnum3]]]`: additional parameters for random generator

   `num0`: the number of random trees (default 1)
   `num1`: number of internal nodes (approx.)
   `rnum2`: probability of an internal node
   `rnum3`: decreased probability of an internal node

Generate four gene trees of the size 10: 
```
./urec -R "abcde;g10" -N4 -p
(((d,e),(c,e)),(((a,b),(b,e)),a),a)
(d,((b,e),((c,d),((b,b),(e,c)))),c)
(((e,d),(e,d)),((c,c),(d,(d,a))),c)
(a,((c,(b,b)),e),(((d,a),e),(d,d)))
```

Generate three gene trees of the size 4 with unique leaf labels:
```
./urec -R "abcdefgh;u4" -N3 -p
((d,c),f,e)
(d,(f,c),h)
((b,d),h,g)
```

Generate four species trees:
```
> ./urec -R "abcde;s" -N4 -p
(b,((c,d),a),e)
(((a,e),d),b,c)
(b,(e,c),(d,a))
(c,(e,(d,a)),b)
```

Generate a species tree with 40 leaves (automatic labels):
```
> ./urec -R "40;s" -p
((((((c,z),(((h1,g1),b),f)),(i,m1)),k1),(e,n)),(d1,(v,i1)),(((m,(j1,q)),((((a,r),(y,(j,(p,k)))),d),(((n1,w),x),(((s,h),f1),(o,(t,a1)))))),(l,(e1,((b1,l1),(u,(g,c1)))))))
```

### Event weights (-D, -L)

`-D weight`: set weight of gene duplications, default is 1.0.

`-L weight`: set weight of gene losses, default is 1.0.

### Computing unrooted reconciliation costs: -u SUBOPTs 

#### For every unrooted gene tree and a species tree (details of costs):

`m`: print the optimal mutation cost, i.e., sum of weighted 
duplications and losses

`o`: print the optimal number of duplication and loss events (D,L); i.e., components of the mutations cost

```
> ./urec -uom -g '((a,a),(b,c),(b,a))' -s '(a,(b,c))' 
(2,1)
3
```

`O`: print one arbitrary optimal rooting of unrooted tree; here the rooting is placed in the middle of the rooting edge.

```
./urec -uoO -g '(a:5,a:3,a:4) ' -s '(a,(b,c))' 
((a:3,a:4):2.5,a:2.5)
(2,0)
```

We recommend to use midpoint plateau rooting strategy `-rmp1` rather than `-uO`:

```
./urec -rmp1 -g '(a:5,a:3,a:4) ' -s '(a,(b,c))' 
(a:4.5,(a:3,a:4):0.5)
```

Print plateau size, i.e., the number of optimal edges (-uP)
```
> ./urec -g "(a,(a,a))"  -s "(a,b)" -uP 
3
```

Print DL-optimal unrooted reconciliation rootings (-uo). An example from https://doi.org/10.1109/TCBB.2013.22:
```
> ./urec -g "((g,(b,c)),((a,h),(f,(e,d))))" -s "(((a,b),c),((d,(e,f)),(g,h)))" -ro
((g,(b,c)),((a,h),(f,(e,d))))
((a,h),((f,(e,d)),(g,(b,c))))
((f,(e,d)),((g,(b,c)),(a,h)))
```

Print D-optimal unrooted reconciliation rootings (-uo -L0). An example from https://doi.org/10.1109/TCBB.2013.22:
```
> ./urec -g "((g,(b,c)),((a,h),(f,(e,d))))" -s "(((a,b),c),((d,(e,f)),(g,h)))" -ro -L0
((g,(b,c)),((a,h),(f,(e,d))))
((a,h),((f,(e,d)),(g,(b,c))))
(f,((e,d),((g,(b,c)),(a,h))))
((e,d),(((g,(b,c)),(a,h)),f))
((f,(e,d)),((g,(b,c)),(a,h)))
```


#### Other unrooted reconciliation options  

`-I`: print all non-root-duplication rooted subtrees
    
```
> ./urec -uI -g '(((a,a),(b,c)),(b,a)) ' -s '(a,(b,c))'
((a,a),(b,c))
(b,a)
```


#### For every species tree, i.e., summary of costs when reconciling a species tree with a set of gene trees.

`c`: print total mutation cost

```
> urec -uc '((a,a),(b,c)); ((b,b),(a,c))' -s '(a,(b,c))'
(a,(b,c))       2
```

`C`: print total cost (D,L)

```
> urec -uC '((a,a),(b,c)); ((b,b),(a,c))' -s '(a,(b,c))'
(a,(b,c))       (2,0)
```

`t`:  print total normalized cost; normalization by duplication diameter
|G|-1

```
> ./urec -ut -g '((a,a),(b,c)); ((b,b),(a,c))' -s '(a,(b,c))'
(a,(b,c))       0.666667
```


##### Event distributions (-ud, -ux)

`d`: print distributions of events per each subtree of the species tree

```
> ./urec -ud -g '((a,a),(b,c)); ((b,b),(a,c))' -s '(a,(b,c))'
(0,0) : (a,(b,c))
(1,0) : a
(0,0) : (b,c)
(1,0) : b
(0,0) : c
```

`-x`: print event distributions embedded into the species tree (nested parenthesis notation with attributes)
    
```
> urec -ux -g '((a,a),(b,b)); ((b,c),(b,c))' -s '(a,(b,c))' -x
(a dup(1) loss(0),(b dup(1) loss(0),c dup(0) loss(1)) dup(1) loss(0)) dup(0) loss(0)
```




### Nearest neighbour interchange (NNI)

`-a n`:   print all NNI neighbours

Print rooted NNI neighbours (-s/-S):
```
> urec -an -s '(a,(b,c))'
((a,c),b)
((a,b),c)
```

Print unrooted NNI neighbours (-g/-G)
```
> urec -an -g '(a,(b,c),d)'
(a,(b,(c,d)))
(a,((b,d),c))
```

### Graphviz

`-Mg`:  graphviz output for gene trees:

```
> urec -Mg -g '((a1:1,a2:2):2,a3:2)' -p | neato -Tpdf > gtree.pdf
> urec -Mg -g '((a1:1,a2:2):2,a3:2,a4:1)' -p | neato -Tpdf > gtree.pdf
```


### Matching rules between gene tree labels and species tree labels (-m)

`-m pNUM`: species name in the first NUM characters of gene ids
`-m p-NUM`: species name in the last NUM characters of gene ids
`-m aDELIM`: species name after delimiter DELIM
`-m bDELIM`: species name before delimiter DELIM

Species name in the first character:
```
> urec -mp1 -b -o -g "(a1,a2,a100)" -s "(a,(b,c))" 
(2,0)
```

Species name in the last two characters:
```
> urec -mp-2 -b -o -g "(1430_aa,091_bb,31_aa)" -s "(aa,bb)" 
(1,0)
```

Species name after \_:
```
> urec -ma_ -b -o -g "(1430_aa,091_bb,31_aa)" -s "(aa,bb)" 
(1,0)
```



### Advanced options 

#### Computing data for unrooted episode clustering (-a54)

```
./urec -T epistests/double.txt -a 54
D (b,((d,a),((g,(e,h)),(f,c)))) 
D ((d,a),(((g,(e,h)),(f,c)),b)) 
D ((b,(d,a)),((g,(e,h)),(f,c))) 
D (g,((e,h),((f,c),(b,(d,a))))) 
D ((e,h),(((f,c),(b,(d,a))),g)) 
D ((g,(e,h)),((f,c),(b,(d,a)))) 
D ((f,c),((b,(d,a)),(g,(e,h))))
```

```
./urec -T epistests/empty5edges.txt -a 54
E ((((b,g),f),a),(d,(e,c))) 
1 (a,((d,(e,c)),((b,g),f))) 
2 (((b,g),f),(a,(d,(e,c)))) 
3 ((e,c),((((b,g),f),a),d)) 
4 (d,((e,c),(((b,g),f),a)))
```

```
./urec -T epistests/empty3edges.txt -a 54
E ((a,e),(c,(b,d))) 
1 ((b,d),((a,e),c)) 
2 (c,((b,d),(a,e))) 
```

```
./urec  -g '(((e,(d,a)),f),((g,c),b))' -s '(f,(c,(((b,g),(a,e)),d)))' -a 54
E ((((g,c),b),(e,(d,a))),f) 
1 ((e,(d,a)),(f,((g,c),b))) 
1 (e,((d,a),(f,((g,c),b)))) 
1 ((d,a),((f,((g,c),b)),e)) 
2 (((g,c),b),((e,(d,a)),f)) 
2 ((g,c),(b,((e,(d,a)),f))) 
2 (b,(((e,(d,a)),f),(g,c))) 
```
### Print candidates for episode rooting (up to 5 edges) (-a5)

```
> ./urec  -T epistests/empty5edges.txt  -a5
((e,c),((((b,g),f),a),d)) 
(((b,g),f),(a,(d,(e,c)))) 
(a,((d,(e,c)),((b,g),f))) 
((((b,g),f),a),(d,(e,c))) 
(((e,c),(((b,g),f),a)),d) 
```

```
> ./urec  -T epistests/empty3edges.txt  -a5
((a,e),(c,(b,d))) 
(c,((b,d),(a,e))) 
(((a,e),c),(b,d)) 
```

```
> ./urec  -T epistests/double.txt  -a5
((b,(d,a)),((g,(e,h)),(f,c)))
```
### Print one rooting on empty edge or nothing (-a51)

```
./urec  -T epistests/empty3edges.txt -a51
((a,e),(c,(b,d)))
```

### Print the rooting on double edge or max 2 candidates for empty edge gene trees (-a52)

```
./urec  -T epistests/empty3edges.txt  -a52
((b,d),((a,e),c)) 
```

### Print all rootings with types: D-double, E-empty, L-leftempty, R-rightempty (-a53)

```
> ./urec  -T epistests/empty3edges.txt  -a53
E ((a,e),(c,(b,d))) 
R ((b,d),((a,e),c)) 
```

### Cluster representation of DL-plateaus (cost weights are ignored)

```
./urec -T  epistests/empty3edges.txt -a2 
GENE TREE: 
Optimal cost: (1,3)
Plateau size: 1
Root S ((a,e),(c,(b,d))) ((d,(b,c)),(a,e))
Bor S (a,e) (a,e)
Bor D (c,(b,d)) (d,(b,c))
Ext L b b
Ext L d d
Ext L a a
Ext L e e
Ext L c c
Ext S (b,d) (d,(b,c))
```

### Cluster representation of D-plateaus (cost weights are ignored)
  
```
./urec -T epistests/empty3edges.txt -a24 
GENE TREE: 
Optimal cost: (1,3)
Plateau size: 1
Root S ((a,e),(c,(b,d))) ((d,(b,c)),(a,e))
Bor S (a,e) (a,e)
Plt D (c,(b,d)) (d,(b,c))
Bor L c c
Plt S ((b,d),(a,e)) ((d,(b,c)),(a,e))
Plt S ((a,e),c) ((d,(b,c)),(a,e))
Bor S (b,d) (d,(b,c))
Ext L b b
Ext L d d
Ext L a a
Ext L e e
```

### Plateau sizes (weights ignored)

`-a6` - DL-plateau

`-a7` - D-plateau

### Print plateau sizes for D and DL, and all rootings with event counts; weights ignored (-a4)

```  
> ./urec -g '(a:2,b:3,a:1)'  -s "(b,(c,a))"  -a4 -D0 -L0
GENE TREE: 
Optimal cost: (1,1)
DL-plateau size: 1
D-plateau size: 3
(a:2,(b:3,a:1):2) 1 3 4
(b:3,(a:1,a:2):3) 1 1 2
((a:2,b:3):1,a:1) 1 3 4
```

### Print edges connectivity graph, i.e., nodes = edges (rooting ids); edge = if two edges are adjacent
```
> ./urec -g '(a:2,b:3,(d:1,(e,f)))' -rc
2 0
1 0
1 2
8 2
5 2
5 8
7 8
6 8
6 7
```
### Test whether there is an empty edge not-incident to a leaf (-a81)

```
./urec -T epistests/empty3edges.txt -a81
Internal empty edge
```

### Options to print gene tree data structure (debug)

`-a1` - DL-plateau

`-a3` - D-plateau




### Old versions of urec

http://bioputer.mimuw.edu.pl/~gorecki/urec/
