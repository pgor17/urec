
-----------------------------------------------------------------------

-a 22 

Midpoint rootings by using branch length data.

Output:  id=<ROOTINGID> optimalheight rooting [midpoint] [dlplateau]

Example 1.

./urec -m p1 -g "((a1:2,a2:3):1,a3:7,b:2)" -s "(a,(b,c))" -a 22 -b 

id=0 8 (a1:2,(a2:3,(a3:7,b:2):1):0)
id=1 8 (a2:3,((a3:7,b:2):1,a1:2):0)
id=2 7 ((a1:2,a2:3):1,(a3:7,b:2):0)
id=5 5.5 (a3:5.5,(b:2,(a1:2,a2:3):1):1.5) midpoint
id=6 7 (((a1:2,a2:3):1,a3:7):0,b:2) dlplateau

Example 2.

./urec -m p1 -g "((a1:2,a2:3):1,a3:7,a4:2)" -s "(a,(b,c))" -a 22 -b 

id=0 8 (a1:2,(a2:3,(a3:7,a4:2):1):0) dlplateau
id=1 8 (a2:3,((a3:7,a4:2):1,a1:2):0) dlplateau
id=2 7 ((a1:2,a2:3):1,(a3:7,a4:2):0) dlplateau
id=5 5.5 (a3:5.5,(a4:2,(a1:2,a2:3):1):1.5) dlplateau midpoint
id=6 7 (((a1:2,a2:3):1,a3:7):0,a4:2) dlplateau
q
-----------------------------------------------------------------------

-a 21
DL-plateau midpoint rootings (branch lengths are ignored if present)

Output:  id=<ROOTINGID> depth rooting [dlmidpoint]

Rootings having the maximal depth are mid-point rootings.

Example 1. 

./urec -m p1 -g "((a1,a2),a3,b)" -s "(a,(b,c))" -a 21 -b 

id=6 1  (((a1,a2),a3),b) dlmidpoint

Example 2.

./urec -m p1 -g "((a1,a2),a3,a4)" -s "(a,(b,c))" -a 21 -b 

id=0 1  (a1,(a2,(a3,a4)))
id=1 1  (a2,((a3,a4),a1))
id=2 2  ((a1,a2),(a3,a4)) dlmidpoint
id=5 1  (a3,(a4,(a1,a2)))
id=6 1  (((a1,a2),a3),a4)

Example 3.

./urec -m p1 -g "((a1,a2),(a3,a4),(a5,a6))" -s "(a,(b,c))" -a 21 -b 

id=0 1  (a1,(a2,((a3,a4),(a5,a6))))
id=1 1  (a2,(((a3,a4),(a5,a6)),a1))
id=2 2  ((a1,a2),((a3,a4),(a5,a6))) dlmidpoint
id=5 1  (a3,(a4,((a5,a6),(a1,a2))))
id=6 1  (a4,(((a5,a6),(a1,a2)),a3))
id=7 2  ((a3,a4),((a5,a6),(a1,a2))) dlmidpoint
id=10 2  (((a1,a2),(a3,a4)),(a5,a6)) dlmidpoint
id=13 1  (a5,(a6,((a1,a2),(a3,a4))))
id=14 1  (a6,(((a1,a2),(a3,a4)),a5))



-----------------------------------------------------------------------

-a 23
Midpoint rootings by using branches.

Similar to -a 22 option but the branch lengths are set to 1.0.


-----------------------------------------------------------------------

-a 24 
D-plateau clusters, similar to -a 2


urec  -T epistests/double.txt -b  -a 24 
urec  -T epistests/empty3edges.txt -b  -a 24  
urec  -T epistests/empty5edges.txt -b  -a 24  


GENE TREE: 
Optimal cost: (4,20)
Plateau size: 7
Root D (((g,(e,h)),(f,c)),(b,(d,a))) (((g,((f,b),d)),(e,c)),(h,a))
Bor L b b
Plt D ((d,a),((g,(e,h)),(f,c))) (((g,((f,b),d)),(e,c)),(h,a))
Bor S (d,a) (((g,((f,b),d)),(e,c)),(h,a))
Plt D (((g,(e,h)),(f,c)),b) (((g,((f,b),d)),(e,c)),(h,a))
Plt D (b,(d,a)) (((g,((f,b),d)),(e,c)),(h,a))
Plt D ((g,(e,h)),(f,c)) (((g,((f,b),d)),(e,c)),(h,a))
Bor L g g
Plt D ((e,h),((f,c),(b,(d,a)))) (((g,((f,b),d)),(e,c)),(h,a))
Bor S (e,h) (((g,((f,b),d)),(e,c)),(h,a))
Plt D (((f,c),(b,(d,a))),g) (((g,((f,b),d)),(e,c)),(h,a))
Plt D (g,(e,h)) (((g,((f,b),d)),(e,c)),(h,a))
Plt D ((f,c),(b,(d,a))) (((g,((f,b),d)),(e,c)),(h,a))
Bor S (f,c) ((g,((f,b),d)),(e,c))
Plt D ((b,(d,a)),(g,(e,h))) (((g,((f,b),d)),(e,c)),(h,a))
Ext L d d
Ext L a a
Ext L e e
Ext L h h
Ext L f f
Ext L c c

GENE TREE: 
Optimal cost: (1,3)
Plateau size: 1
Root S ((a,e),(c,(b,d))) ((d,(b,c)),(a,e))
Bor S (a,e) (a,e)
Plt D (c,(b,d)) (d,(b,c))
Bor L c c
Plt S ((b,d),(a,e)) ((d,(b,c)),(a,e))
Plt S ((a,e),c) ((d,(b,c)),(a,e))
Bor S (b,d) (d,(b,c))
Ext L b b
Ext L d d
Ext L a a
Ext L e e

GENE TREE: 
Optimal cost: (2,7)
Plateau size: 1
Root S ((((b,g),f),a),(d,(e,c))) ((f,((a,g),b)),(e,(d,c)))
Bor S (e,c) (e,(d,c))
Plt S ((((b,g),f),a),d) ((f,((a,g),b)),(e,(d,c)))
Bor S ((b,g),f) (f,((a,g),b))
Plt S (a,(d,(e,c))) ((f,((a,g),b)),(e,(d,c)))
Bor L a a
Plt S ((d,(e,c)),((b,g),f)) ((f,((a,g),b)),(e,(d,c)))
Plt D (((b,g),f),a) (f,((a,g),b))
Plt D (d,(e,c)) (e,(d,c))
Plt S ((e,c),(((b,g),f),a)) ((f,((a,g),b)),(e,(d,c)))
Bor L d d
Ext L e e
Ext L c c
Ext L b b
Ext L g g
Ext S (b,g) ((a,g),b)
Ext L f f



---------------------------------------------------
UME option: -a 54

./urec -T epistests/double.txt  -b -a 54
D (b,((d,a),((g,(e,h)),(f,c)))) 
D ((d,a),(((g,(e,h)),(f,c)),b)) 
D ((b,(d,a)),((g,(e,h)),(f,c))) 
D (g,((e,h),((f,c),(b,(d,a))))) 
D ((e,h),(((f,c),(b,(d,a))),g)) 
D ((g,(e,h)),((f,c),(b,(d,a)))) 
D ((f,c),((b,(d,a)),(g,(e,h))))


./urec -T epistests/empty5edges.txt -b -a 54
E ((((b,g),f),a),(d,(e,c))) 
1 (a,((d,(e,c)),((b,g),f))) 
2 (((b,g),f),(a,(d,(e,c)))) 
3 ((e,c),((((b,g),f),a),d)) 
4 (d,((e,c),(((b,g),f),a)))


./urec -T epistests/empty3edges.txt -b -a 54
E ((a,e),(c,(b,d))) 
1 ((b,d),((a,e),c)) 
2 (c,((b,d),(a,e))) 


./urec  -g '(((e,(d,a)),f),((g,c),b))' -s '(f,(c,(((b,g),(a,e)),d)))' -b  -a 54
E ((((g,c),b),(e,(d,a))),f) 
1 ((e,(d,a)),(f,((g,c),b))) 
1 (e,((d,a),(f,((g,c),b)))) 
1 ((d,a),((f,((g,c),b)),e)) 
2 (((g,c),b),((e,(d,a)),f)) 
2 ((g,c),(b,((e,(d,a)),f))) 
2 (b,(((e,(d,a)),f),(g,c))) 



