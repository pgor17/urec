
/************************************************************************
 Unrooted REConciliation version 1.00
 (c) Copyright 2005-2006 by Pawel Gorecki
 Written by P.Gorecki.
 Permission is granted to copy and use this program provided no fee is
 charged for it and provided that this copyright notice is not removed.
 *************************************************************************/

#include <ctype.h>
#include <iostream>
#include <cstdlib>
#include <cstring>

using namespace std;

#include "tree.h"
#include "rtree.h"

double weight_loss = 1.0;
double weight_dup = 1.0;
extern int rsort;

int getspecies(char *s,int len);

int usespecnames=0;

string getspecname(int specid,char *name)
{
  if ((!usespecnames) && name)
    return string(name);
  return specnames[specid];
}

int INPLINE=1;
int INPPOS=0;
string INPFILE("");

void initParsing(string file)
{
	INPPOS=0;
	INPLINE=0;
	INPFILE=file;
}

#define alfnumextchar(x) (isalnum(x) || x=='_' || x=='.' || x=='+' || x=='-')
char* _getTok(char *s, int &p, char &toktype)
{  

  while (isspace(s[p])) { 
  	if (s[p]=='\n') { INPLINE++; INPPOS=p+1; }
  	p++;
  }
  char *cur = s+p;


  if (alfnumextchar(s[p]))
    {
      while (alfnumextchar(s[p])) p++;
      toktype=TLABEL;             
      return cur;

    }

  if ((s[p]=='(')  || (s[p]==')') || (s[p]==',') || s[p]==':' || s[p]==';')
    {
      toktype=s[p];
      p++;      
      return cur;
    }
  
  if (!s[p]) toktype=TEOF;
  else toktype=TERR;  
  return NULL; // EOF or err
  
}


char* expectTok(char *s, int &p, char exptype, char &found)
{	
	int prev=p;
	int oINPLINE=INPLINE, oINPPOS=INPPOS;
	char *r = _getTok(s,p,found);
	if (found!=exptype)	
	{
		INPLINE=oINPLINE;
		INPPOS=oINPPOS;
		p=prev; // go back
		return NULL;
	}
	return r;	
}

char checkTok(char *s, int p)
{
	int prev=p;
	int oINPLINE=INPLINE, oINPPOS=INPPOS;
	char toktype;
	_getTok(s,p,toktype);	
	INPLINE=oINPLINE;
	INPPOS=oINPPOS;
	p=prev;
	return toktype;
}

char* nextTok(char *s, int &p)
{
	char toktype;
	return _getTok(s,p,toktype);
}



double parseNum(char *s, int &p, int forced, int &fnd)
{
	double v=-10;
	char toktype;
	fnd=0;
		
	char *cur = expectTok(s,p,TLABEL,toktype); 
	if (!cur)	
		if (!forced)  return 0; 

	if (sscanf(cur,"%lf",&v)==1)
	{
		fnd=1; 
		return v;
	}

	if (forced)
		parseError("number expected");		
		

	return 0;
}

double parseBranchLen(char *s,int &p)
{
	int fnd=0;
	nextTok(s,p); // eat :
	return  parseNum(s,p,1,fnd);
}

iterator_tree::iterator_tree(RTree *tr, int flag_) {
	t = tr;
	c = tr->root();
	flag = flag_;
}

// virtual RNode *parseNode(char *s, int &p, int num);
// virtual RNode *createLeaf(int species, double branchlen) { return new RLeaf(species,0,branchlen); }
// virtual RNode *createInt(RNode *a, RNode *b, double branchlen) { return new RInt(a,b,branchlen); } 


RNode *parseNode(char *s,int &p)
{	
	char toktype = checkTok(s,p);
	char fndtype;
	int fnd=0;
	

	if (toktype=='(')
	{
		
		nextTok(s,p); // eat (

		RNode *a = parseNode(s,p);
		if (!expectTok(s,p,',',fndtype)) parseError("exected ,");
		
		
		RNode *b = parseNode(s,p);
		if (!expectTok(s,p,')',fndtype))
			parseError("exected )");


		if (rsort && (a->minlab() >= b->minlab())) 
		{
			RNode *x = a;
			a=b;
			b=x;
		}
	
		parseNum(s,p,0,fnd); // ignore number 

		if (checkTok(s,p)==':') 		 
			return new RInt(a,b,parseBranchLen(s,p));

		return new RInt(a,b);		
	}
	// read label

	char *cur=expectTok(s,p,TLABEL,fndtype);
	if (cur==NULL)
		parseError("exected label");
		
	int spid = getspecies(cur,s+p-cur);

	parseNum(s,p,0,fnd); // ignore bootstrap 

	if (checkTok(s,p)==':') 
		return new RLeaf(spid,0,parseBranchLen(s,p));

	return new RLeaf(spid,0);
}

RTree::RTree(char *fs, int num, string tn, double _weight) : Tree(tn,_weight)
{
	int px=0;
	rootn=parseNode(fs,px);
	rootn->depth(0);
	treename=tn;	
}

RNode *iterator_tree::operator()() {
	RNode *res = step();
	while (res) {
		if (flag & F_ALL)
			break;
		if ((flag & F_INTERNAL) && (!res->leaf()))
			break;
		if ((flag & F_LEAVES) && (res->leaf()))
			break;
		res = step();
	}
	return res;
}

RNode *iterator_tree::step() {
	RNode *res = c;
	if (!c)
		return c; // finished
	if (!c->leaf())
		c = ((RInt*) c)->l();
	else {
		while (c) {
			RNode *prev = c;
			c = c->p();
			if (!c)
				break; // last
			if ((((RInt*) c)->l()) == prev) {
				c = ((RInt*) c)->r();
				break;
			}
		}
	}
	return res;
}

RNode *SpeciesTree::lca(RNode *a, RNode *b) {
	if (b->isParentOf(a))
		return b;
	while (a) {
		if (a->isParentOf(b))
			return a;
		a = a->p();
	}
	return NULL;
}

RNode *RNode::isParentOf(RNode *c) {
	while (c) {
		if (c == this)
			return c;
		c = c->p();
	}
	return 0;
}

char* xstrndup(const char *s, int len) {
	if (len == 0)
		return strdup(s);
	char *b = new char[len + 1];
	strncpy(b, s, len);
	b[len] = 0;
	return b;
}

int getspecies(char *s,int len)
{

	char old=' ';
	int num;

	if (len)
	{
		old=s[len];
		s[len]=0;
	}
	
	if (specnames2id.count(s)==0)
	{
		specnames2id[s]=num=specnames.size();
		specorder.push_back(num);
		specnames.push_back(s);
		//cout << "new species: #" << s << endl;
	}
	else num=specnames2id[s];
	if (len) s[len]=old;

	return num;
}
